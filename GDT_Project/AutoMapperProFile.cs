﻿using AutoMapper;
using GDT_Project.DTOs;
using GDT_Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project
{
    public class AutoMapperProFile : Profile
    {
        public AutoMapperProFile()
        {
            CreateMap<Hero, HeroGetDto>();
            CreateMap<HeroAddDto, Hero>();
            CreateMap<HeroUpdateDto, Hero>();
        }
    }
}