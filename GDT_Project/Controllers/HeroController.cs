﻿using GDT_Project.DTOs;
using GDT_Project.Models;
using GDT_Project.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Controllers
{
    //TEST
    [Authorize]
    [ApiController]
    [Route("hero")]
    public class HeroController : ControllerBase
    {
        private readonly IHeroService _heroService;

        public HeroController(IHeroService heroService)
        {
            _heroService = heroService;
        }

        [HttpGet("getall")]//hero/getall
        [ResponseCache(Duration = 30)]
        public async Task<IActionResult> GET()
        {
            var result = await _heroService.GetHeroes();
            return Ok(result);
        }

        [HttpGet("getallheros")]//hero/getall
        public async Task<IActionResult> GetAllHeros([FromQuery] PaginationDto pagination)
        {
            var result = await _heroService.GetAllHeroes(pagination);
            return Ok(result);
        }

        //[HttpGet("test")]//hero/getall
        //[ResponseCache(Duration = 30)]
        //public ActionResult kkk()
        //{
        //    return Ok("a");
        //}

        [HttpGet("{id}")]//hero/{id}
        public async Task<IActionResult> GetByID(int id)
        {
            //await Task.Yield();
            var hero = await _heroService.GetByID(id);
            if (hero == null)
            {
                return NotFound();
            }
            return Ok(hero);
        }

        [HttpPost("addhero")]
        public async Task<IActionResult> Addhero(HeroAddDto heros)
        {
            //await Task.Yield();
            var result = await _heroService.Addhero(heros);
            return Ok(result);
        }

        [HttpPut("updatehero/{heroId}")]
        public async Task<IActionResult> UpdateHero(int heroId, HeroUpdateDto hero)
        {
            //await Task.Yield();
            var result = await _heroService.UpdateHero(heroId, hero);
            return Ok(result);
        }

        [HttpDelete("deletehero/{heroId}")]
        public async Task<IActionResult> DeleteHero(int heroId)
        {
            var result = await _heroService.DeleteHero(heroId);
            return Ok(result);
        }
    }
}