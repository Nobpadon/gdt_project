﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.DTOs
{
    public class HeroAddDto
    {
        public String HeroName { get; set; }
        public int Grade { get; set; }
        public int Level { get; set; }
    }
}
