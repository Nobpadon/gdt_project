﻿namespace GDT_Project.DTOs
{
    public class HeroGetDto
    {
        public int heroid { get; set; }

        public string HeroName { get; set; }

        public int Grade { get; set; }

        public int Level { get; set; }
    }
}
