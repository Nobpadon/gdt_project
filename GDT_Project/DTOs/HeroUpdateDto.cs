﻿namespace GDT_Project.DTOs
{
    using System;

    public class HeroUpdateDto
    {
        public String HeroName { get; set; }

        public int Grade { get; set; }

        public int Level { get; set; }
    }
}
