﻿using GDT_Project.Validations;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Models
{
    public class hero
    {
        public int HeroId { get; set; }
        [Required]
        [StringLength(10)]
        [FirstLetterUppercase]
        public string HeroName { get; set; } 
        public int Grade { get; set; } 
        public int Level { get; set; } 

    }
}
