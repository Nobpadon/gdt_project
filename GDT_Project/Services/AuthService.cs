﻿using GDT_Project.Data;
using GDT_Project.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public class AuthService : IAuthService
    {
        private readonly GDTContext _context;
        private readonly IConfiguration _configuration;

        public AuthService(GDTContext context, IConfiguration configuration)
        {
            this._context = context;
            this._configuration = configuration;
        }

        public async Task<ResponsService<string>> Login(string username, string password)
        {
            ResponsService<string> response = new ResponsService<string>();

            User user = await _context.User.FirstOrDefaultAsync(x => x.UserName.ToLower() == username.ToLower());
            if (user == null)
            {
                response.IsSuccess = false;
                response.Message = "Username not found.";
                return response;
            }

            if (!VerifyPasswordHash(password, user.PasswordHash, user.PasswordSalt))
            {
                response.IsSuccess = false;
                response.Message = "incorrect Password.";
                return response;
            }

            //response.Data = user.UserId.ToString();

            response.Data = CreateToken(user);
            return response;
        }

        public async Task<ResponsService<int>> Register(User user, string password)
        {
            ResponsService<int> response = new ResponsService<int>();
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);

            if (await UserExists(user.UserName))
            {
                response.IsSuccess = false;
                response.Message = "User already exists.";
                return response;
            }

            user.PasswordHash = passwordHash;
            user.PasswordSalt = passwordSalt;

            _context.User.Add(user);
            await _context.SaveChangesAsync();
            response.Data = user.UserId;

            return response;
        }

        public async Task<bool> UserExists(string username)
        {
            bool result = await _context.User.AnyAsync(o => o.UserName.ToLower() == username.ToLower());
            if (!result)
            {
                return false;
            }
            return true;
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512())
            {
                passwordSalt = hmac.Key;
                byte[] b = Encoding.UTF8.GetBytes(password);
                passwordHash = hmac.ComputeHash(b);
            }
        }

        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new HMACSHA512(passwordSalt))
            {
                var computeHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
                for (int i = 0; i < computeHash.Length; i++)
                {
                    if (computeHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
                return true;
            }
        }

        private string CreateToken(User user)
        {
            List<Claim> claims = new List<Claim>
        {
            new Claim(ClaimTypes.NameIdentifier,user.UserId.ToString()),
            new Claim(ClaimTypes.Name, user.UserName)
        };
            var secretKey = _configuration.GetSection("AppSettings:Token").Value;
            SymmetricSecurityKey key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(secretKey));

            SigningCredentials creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            SecurityTokenDescriptor tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(1),
                SigningCredentials = creds
            };

            JwtSecurityTokenHandler tokenHandler = new JwtSecurityTokenHandler();

            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);

            return tokenHandler.WriteToken(token);
        }
    }
}