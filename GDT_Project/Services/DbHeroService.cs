﻿using AutoMapper;
using GDT_Project.Data;
using GDT_Project.DTOs;
using GDT_Project.Entities;
using GDT_Project.Helpers;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public class DbHeroService : IHeroService
    {
        private readonly GDTContext _context;
        private readonly IMapper _mapper;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public DbHeroService(GDTContext context, IMapper mapper, IHttpContextAccessor httpContextAccessor)
        {
            _context = context;
            _mapper = mapper;
            this._httpContextAccessor = httpContextAccessor;
        }

        public async Task<ResponsService<List<HeroGetDto>>> Addhero(HeroAddDto newHero)
        {
            var response = new ResponsService<List<HeroGetDto>>();
            Hero hero = _mapper.Map<Hero>(newHero);

            _context.Hero.Add(hero);
            await _context.SaveChangesAsync();

            List<Hero> heroes = await _context.Hero.ToListAsync();
            List<HeroGetDto> heroDto = _mapper.Map<List<HeroGetDto>>(heroes);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponsService<List<HeroGetDto>>> DeleteHero(int heroId)
        {
            var response = new ResponsService<List<HeroGetDto>>();
            Hero hero = await _context.Hero.FirstOrDefaultAsync(x => x.HeroId == heroId);
            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this heroid {heroId} Not Found";
                return response;
            }

            _context.Remove(hero);
            await _context.SaveChangesAsync();

            List<Hero> heroes = await _context.Hero.ToListAsync();

            List<HeroGetDto> heroDto = _mapper.Map<List<HeroGetDto>>(heroes);
            response.Data = heroDto;
            return response;
        }

        public async Task<ResponsService<HeroGetDto>> GetByID(int id)
        {
            var response = new ResponsService<HeroGetDto>();

            Hero hero = await _context.Hero.FirstOrDefaultAsync(x => x.HeroId.Equals(id));

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this heroid {id} not found";
            }
            else
            {
                HeroGetDto heroDto = _mapper.Map<HeroGetDto>(hero);
                response.Data = heroDto;
            }

            return response;
        }

        public async Task<ResponsService<List<HeroGetDto>>> GetHeroes()
        {
            var response = new ResponsService<List<HeroGetDto>>();
            var heros = await _context.Hero.AsNoTracking().ToListAsync();

            List<HeroGetDto> heroDto = _mapper.Map<List<HeroGetDto>>(heros);

            response.Data = heroDto;

            return response;
        }

        public async Task<ResponsService<HeroGetDto>> UpdateHero(int heroID, HeroUpdateDto updateDto)
        {
            var response = new ResponsService<HeroGetDto>();
            Hero hero = await _context.Hero.FirstOrDefaultAsync(x => x.HeroId == heroID);
            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this heroid  {heroID} Not Found";
                return response;
            }
            hero.HeroName = updateDto.HeroName;
            hero.Grade = updateDto.Grade;
            hero.Level = updateDto.Level;

            await _context.SaveChangesAsync();
            HeroGetDto heroDto = _mapper.Map<HeroGetDto>(hero);
            response.Data = heroDto;
            return response;
        }

        public async Task<ResponsService<List<HeroGetDto>>> GetAllHeroes(PaginationDto paginationDto)
        {
            var response = new ResponsService<List<HeroGetDto>>();

            var queryable = _context.Hero.AsQueryable();
            await _httpContextAccessor.HttpContext.InsertPaginationParametersInResponse(queryable, paginationDto.RecordsPerPage, queryable.Count(), paginationDto.Page);
            var lstHeroes = await queryable.Paginate(paginationDto).ToListAsync();

            List<HeroGetDto> heroGetDtos = _mapper.Map<List<HeroGetDto>>(lstHeroes);

            response.Data = heroGetDtos;

            return response;
        }
    }
}