﻿using AutoMapper;
using GDT_Project.DTOs;
using GDT_Project.Models;
using Microsoft.AspNetCore.Mvc.ViewComponents;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public class HeroService : IHeroService
    {
        private readonly IMapper _mapper;
        private List<hero> _Heroes;

        public HeroService(IMapper mapper)
        {
            List<hero> Heros = new List<hero>
            {
                new hero(),
                new hero () {HeroId =2,HeroName="Jont"},
                new hero () {HeroId =3,HeroName="Best" },
                new hero () {HeroId =4,HeroName="tam" },
                new hero () {HeroId =5,HeroName="Ton" }
            };
            _Heroes = Heros;
            _mapper = mapper;
        }

        public ResponsService<List<HeroGetDto>> Addhero(HeroAddDto newHero)
        {
            // await Task.Yield();
            hero hero = _mapper.Map<hero>(newHero);

            hero.HeroId = _Heroes.Max(x => x.HeroId + 1);

            _Heroes.Add(hero);
            var heroDto = _mapper.Map<List<HeroGetDto>>(_Heroes);

            var response = new ResponsService<List<HeroGetDto>>();
            response.Data = heroDto;

            return response;
        }

        public ResponsService<HeroGetDto> GetByID(int id)
        {
            var response = new ResponsService<HeroGetDto>();

            var hero = _Heroes.FirstOrDefault(x => x.HeroId.Equals(id));

            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"this heroid {id} not found";
            }
            else
            {
                HeroGetDto heroDto = _mapper.Map<HeroGetDto>(hero);
                response.Data = heroDto;
            }

            return response;
        }

        public ResponsService<List<HeroGetDto>> GetHeroes()
        {
            var response = new ResponsService<List<HeroGetDto>>();

            List<HeroGetDto> heroDto = _mapper.Map<List<HeroGetDto>>(_Heroes);
            response.Data = heroDto;

            return response;
        }

        public ResponsService<HeroGetDto> UpdateHero(int heroId, HeroUpdateDto updateDto)
        {
            var response = new ResponsService<HeroGetDto>();
            hero hero = _Heroes.FirstOrDefault(x => x.HeroId == heroId);
            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = "Not Found";
                return response;
            }
            hero.HeroName = updateDto.HeroName;
            hero.Grade = updateDto.Grade;
            hero.Level = updateDto.Level;
            HeroGetDto heroDto = _mapper.Map<HeroGetDto>(hero);
            response.Data = heroDto;
            return response;
        }

        public ResponsService<List<HeroGetDto>> DeleteHero(int heroId)
        {
            var response = new ResponsService<List<HeroGetDto>>();
            hero hero = _Heroes.FirstOrDefault(x => x.HeroId == heroId);
            if (hero == null)
            {
                response.IsSuccess = false;
                response.Message = $"Not Found{heroId}";
                return response;
            }

            _Heroes.Remove(hero);

            List<HeroGetDto> heroDto = _mapper.Map<List<HeroGetDto>>(_Heroes);
            response.Data = heroDto;
            return response;
        }

        public ResponsService<List<HeroGetDto>> GetAllHeroes(PaginationDto paginationDto)
        {
            throw new NotImplementedException();
        }
    }
}