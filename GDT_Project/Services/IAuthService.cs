﻿using GDT_Project.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public interface IAuthService
    {
        Task<ResponsService<int>> Register(User user, string password);

        Task<ResponsService<string>> Login(string username, string password);

        Task<bool> UserExists(string username);
    }
}