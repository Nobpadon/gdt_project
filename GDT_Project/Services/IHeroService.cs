﻿using GDT_Project.DTOs;
using GDT_Project.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GDT_Project.Services
{
    public interface IHeroService
    {
        public ResponsService<List<HeroGetDto>> GetHeroes();

        public ResponsService<List<HeroGetDto>> GetAllHeroes(PaginationDto paginationDto);

        public ResponsService<HeroGetDto> GetByID(int id);

        public ResponsService<HeroGetDto> UpdateHero(int heroID, HeroUpdateDto updateDto);

        public ResponsService<List<HeroGetDto>> Addhero(HeroAddDto newHero);

        public ResponsService<List<HeroGetDto>> DeleteHero(int heroId);
    }
}